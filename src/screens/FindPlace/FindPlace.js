import React ,{Component} from 'react';
import {View,Text,TouchableOpacity,StyleSheet,Animated} from 'react-native';
import {connect} from 'react-redux';
import {getPlace} from '../../store/actions/index'
import PlaceList from '../../component/PlaceList/PlaceList'
class FindPlaceScreen extends Component{
    static navigatorStyle ={
        navBarButtonColor:"orange"
    }
    state={
        placesLoaded:false,
        removeAnim: new Animated.Value(1),
        placesAnim: new Animated.Value(0)
    }
    placesLoadedHandler = () => {
        Animated.timing(this.state.placesAnim, {
          toValue: 1,
          duration: 500,
          useNativeDriver: true
        }).start();
      };
       placesSearcHandler =()=>{
          Animated.timing(this.state.removeAnim,{
              toValue:0,
              duration:500,
              useNativeDriver:true,
              
          } ).start(() => {
            this.setState({
              placesLoaded: true
            });
            this.props.onLoadPlaace();
        })
       }

    constructor(props){
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
        
    }
    
   
   onNavigatorEvent = event =>{
      if(event.type==="NavBarButtonPress"){
          if(event.id==="sideDrawerToggle"){
              this.props.navigator.toggleDrawer(
                  {
                      side:"left"
                  }
              );
          }
      }
   }

   componentDidMount(){
    this.props.onLoadPlaace();
    this.placesSearcHandler()
}
    itemSelecterdHandlert =(key)=>{
        const selPlace =this.props.places.find(place=>{
            return place.key ===key;
        });
        this.setState({
            placesLoaded: false
          });
        this.props.navigator.push({
            screen :"awesome-places.PlaceDEtailScreen",
            title:selPlace.name,
            passProps:{
                selectedPlace:selPlace
            }
        })

    }
render (){
   
    let content =(
        <Animated.View 
        style={{
            opacity:this.state.removeAnim,
            transform:[
                {
                    scale: this.state.removeAnim.interpolate({
                        inputRange: [0,1],
                        outputRange:[12,1]
                    }
                       
                    )
                }
            ]
        }}>
           <TouchableOpacity onPress= {this.placesSearcHandler}>
           <View style={styles.searchButton} >
               <Text style={styles.searchButtonText}> heloo</Text>
               </View>
           </TouchableOpacity>
           </Animated.View>
    );
    if(this.state.placesLoaded){
        content=(
            <PlaceList places={this.props.places} 
            onItemSelected ={this.itemSelecterdHandlert}/>
        )
    }
    return(
       <View style={this.state.placesLoaded?null:styles.buttonContainer}>
           {content}
        
       </View>
    );
}
} 
const styles = StyleSheet.create({
    buttonContainer:{
     flex:1,
     justifyContent:"center",
     alignItems:"center"
    },
    searchButton:{
        borderColor:"orange",
        borderWidth:3,
        borderRadius:50,
        padding:20
    },
    searchButtonText:{
        color:"orange",
        fontWeight:"bold",
        fontSize:26
    }
})
const mapStateToProps = state => {
    return{
        places:state.places.places
    };
}
const mapDispatchToProps =dispatch =>{
    return{
        onLoadPlaace:()=>dispatch(getPlace())
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(FindPlaceScreen);