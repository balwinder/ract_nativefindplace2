import React,{Component} from 'react';
import {View,Text,Dimensions,StyleSheet,Button} from 'react-native';

class SideDrwaer1 extends Component{
    render(){
        return(
            <View style={[styles.container,{width:Dimensions.get("window").width*0.8}]}>
                <Text>SideDrawe1</Text>
                <Button title="Logout"/>
                </View>
        );
    }
}
const styles = StyleSheet.create({
    container:{
         padding:22,
         backgroundColor:"white",
         flex:1
    }
})
export default SideDrwaer1 