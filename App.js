import {Navigation} from 'react-native-navigation';
//register Screens
import AuthScreen from './src/screens/Auth/Auth';
import SharePlace from './src/screens/SharePlace/SharePlace';
import FindPlace from './src/screens/FindPlace/FindPlace';
import {Provider} from 'react-redux';
import configureStore from "./src/store/configureStore";
import placeDetailScreen from './src/screens/PlaceDetail/PlaceDetail';
import SideDrawr from './src/screens/SideDrawer/SideDrawer';
import SideDrawr1 from './src/screens/SideDrawer1/SideDrwaer1';
const store = configureStore();


Navigation.registerComponent("awesome-places.AuthScreen",()=>AuthScreen,store,Provider);
Navigation.registerComponent("awesome-places.SharePlace",()=>SharePlace,store,Provider);
Navigation.registerComponent("awesome-places.FindPlace",()=>FindPlace,store,Provider);
Navigation.registerComponent("awesome-places.PlaceDEtailScreen",()=>placeDetailScreen,store,Provider);
Navigation.registerComponent("awesome-places.SideDrawr",()=>SideDrawr);
Navigation.registerComponent("awesome-places.SideDrawr1",()=>SideDrawr1);
//stat app

Navigation.startSingleScreenApp({
  screen :{
    screen:"awesome-places.AuthScreen" ,
    title:"Login" 
  }
})